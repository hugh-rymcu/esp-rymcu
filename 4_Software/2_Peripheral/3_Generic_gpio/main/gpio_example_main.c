/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

/**
 * Brief:
 * This test code shows how to configure gpio and how to use gpio interrupt.
 *
 * GPIO status:
 * GPIO18: output (ESP32C2/ESP32H2 uses GPIO8 as the second output pin)
 * GPIO19: output (ESP32C2/ESP32H2 uses GPIO9 as the second output pin)
 * GPIO4:  input, pulled up, interrupt from rising edge and falling edge
 * GPIO5:  input, pulled up, interrupt from rising edge.
 *
 * Note. These are the default GPIO pins to be used in the example. You can
 * change IO pins in menuconfig.
 *
 * Test:
 * Connect GPIO18(8) with GPIO4
 * Connect GPIO19(9) with GPIO5
 * Generate pulses on GPIO18(8)/19(9), that triggers interrupt on GPIO4/5
 *
 */

#define GPIO_OUTPUT_IO_0 CONFIG_GPIO_OUTPUT_0
#define GPIO_OUTPUT_IO_1 CONFIG_GPIO_OUTPUT_1
#define GPIO_OUTPUT_PIN_SEL ((1ULL << GPIO_OUTPUT_IO_0) | (1ULL << GPIO_OUTPUT_IO_1))
#define GPIO_INPUT_IO_0 CONFIG_GPIO_INPUT_0
#define GPIO_INPUT_IO_1 CONFIG_GPIO_INPUT_1
#define GPIO_INPUT_PIN_SEL ((1ULL << GPIO_INPUT_IO_0) | (1ULL << GPIO_INPUT_IO_1))
#define ESP_INTR_FLAG_DEFAULT 0

static QueueHandle_t gpio_evt_queue = NULL;

static void IRAM_ATTR gpio_isr_handler(void *arg)
{
    uint32_t gpio_num = (uint32_t)arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void gpio_task_example(void *arg)
{
    uint32_t io_num;
    for (;;)
    {
        if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            printf("GPIO[%" PRIu32 "] intr, val: %d\n", io_num, gpio_get_level(io_num));
        }
    }
}

void app_main(void)
{
    /***************************************************************************************************
     *  初始化GPIO
     *
     ***************************************************************************************************/
    // zero-initialize the config structure.
    gpio_config_t io_conf = {
        .intr_type = GPIO_INTR_DISABLE,      // disable interrupt
        .mode = GPIO_MODE_OUTPUT,            // set as output mode
        .pin_bit_mask = GPIO_OUTPUT_PIN_SEL, // bit mask of the pins that you want to set,e.g.GPIO 8/18
        .pull_down_en = 0,                   // disable pull - down mode
        .pull_up_en = 0                      // disable pull-up mode
    };
    // configure GPIO with the given settings
    gpio_config(&io_conf);

    gpio_config_t io_conf1 = {
        .intr_type = GPIO_INTR_POSEDGE,     // interrupt of rising edge
        .mode = GPIO_MODE_INPUT,            // set as input mode
        .pin_bit_mask = GPIO_INPUT_PIN_SEL, // bit mask of the pins that you want to set,e.g.GPIO 4/5
        .pull_up_en = 1                     // enable pull-up mode
    };
    gpio_config(&io_conf1);

    // change gpio interrupt type for one pin
    gpio_set_intr_type(GPIO_INPUT_IO_0, GPIO_INTR_ANYEDGE);
    /***************************************************************************************************
     *  创建消息队列，gpio中断事件任务task
     *
     ***************************************************************************************************/
    // create a queue to handle gpio event from isr
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    // start gpio task
    xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);

    // install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    // hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void *)GPIO_INPUT_IO_0);
    // hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_1, gpio_isr_handler, (void *)GPIO_INPUT_IO_1);

    // remove isr handler for gpio number.
    gpio_isr_handler_remove(GPIO_INPUT_IO_0);
    // hook isr handler for specific gpio pin again
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void *)GPIO_INPUT_IO_0);

    printf("Minimum free heap size: %" PRIu32 " bytes\n", esp_get_minimum_free_heap_size());

    int cnt = 0;
    while (1)
    {
        /***************************************************************************************************
         *  每秒输出翻转
         *
         ***************************************************************************************************/
        printf("cnt: %d\n", cnt++);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(GPIO_OUTPUT_IO_0, cnt % 2);
        gpio_set_level(GPIO_OUTPUT_IO_1, (cnt + 1) % 2);

        printf("  ____ ____ _____ _                       _____         _\r\n");
        printf(" / ___|  _ \\_   _(_)_ __ ___   ___ _ __  |_   _|__  ___| |_\r\n");
        printf("| |  _| |_) || | | | '_ ` _ \\ / _ \\ '__|   | |/ _ \\/ __| __|\r\n");
        printf("| |_| |  __/ | | | | | | | | |  __/ |      | |  __/\\__ \\ |_\r\n");
        printf(" \\____|_|    |_| |_|_| |_| |_|\\___|_|      |_|\\___||___/\\__|\r\n");
    }
}
