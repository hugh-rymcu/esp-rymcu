#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/******************************************************************************
 * 图片数组.c文件生成方法：
 * 1.准备.png图片一张
 * 2.访问https://lvgl.io/，菜单栏找到Tools->image conventer
 * 3.点击Browse上传图片，color format设置为CF_TRUE_COLOR,Output format选择C array
 * 4.点击Convert,等待.c文件生成并添加到工程目录。
******************************************************************************/
LV_IMG_DECLARE(LOGO_RYMCU); // 2.图片声明

void my_gui(void)
{
    lv_obj_t *img = lv_img_create(lv_scr_act()); // 1.创建图片部件
    lv_img_set_src(img, &LOGO_RYMCU);            // 2.设置图片源
    lv_obj_center(img);                          // 图片居中

    lv_img_set_offset_x(img, 10); // 3.图片偏移
    lv_img_set_offset_y(img, 10);

    //lv_obj_set_style_img_recolor(img, lv_color_hex(0xFFFF0000), LV_PART_MAIN); // 4.图片重新着色
    //lv_obj_set_style_img_recolor_opa(img, 255, LV_PART_MAIN);

    lv_img_set_zoom(img, 256);  // 5.图片缩放，128=1/2，256=1/1，512=2/1
    lv_img_set_angle(img, 0); // 6.顺时针方向旋转，900=90度

    lv_obj_update_layout(img); // 7.更新原点
    lv_img_set_pivot(img, 0, 0);
}
