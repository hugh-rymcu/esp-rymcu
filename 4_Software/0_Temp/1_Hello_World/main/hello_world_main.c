/*
 * SPDX-FileCopyrightText: 2010-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: CC0-1.0
 */

#include <stdio.h>
#include <inttypes.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_chip_info.h"
#include "esp_flash.h"

#include "lv_port_indev_template.h"
#include "lv_port_disp_template.h"
#include "lvgl.h"
#include "lcd_driver.h"
void event_cb(lv_event_t *e);

void myTask(void *pvParam)
{
    while(1)
    {
        lv_task_handler();
        lv_tick_inc(100);
        vTaskDelay(100/ portTICK_PERIOD_MS); // delay 1s
    }
}
void app_main(void)
{
    xTaskCreate(myTask,"myTask1",8192,NULL,1,NULL);
    // lcd_init();

    // lcd_clear(BLACK);
    //  lcd_set_color(BLACK);
    //  lcd_draw_big_point(120, 160);
    //  lcd_draw_circle(120,160,50);
    //  lcd_draw_line(0,0,240,320);

    lv_init();            // LVGL初始化
    lv_port_indev_init(); // 触摸初始化
    lv_port_disp_init();  // 显示器初始化

    /*Create an object with the new style*/

    lv_obj_t *btn = lv_btn_create(lv_scr_act()); // 1.创建btn部件
    lv_obj_set_size(btn, 100, 50);
    lv_obj_center(btn);

    lv_obj_t *label = lv_label_create(btn); //2.创建标签，并关联btn
    lv_label_set_text(label, "Click me!");
    lv_obj_center(label);

    lv_obj_add_event(btn, event_cb, LV_EVENT_CLICKED, NULL); // 3.添加点击事件

    while (1)
    {
        printf("Hello world!\n");
        //lv_task_handler();
        //lv_tick_inc(10);
        vTaskDelay(1000/ portTICK_PERIOD_MS); // delay 1s
    }
}

void event_cb(lv_event_t *e) // 5.点击事件回调函数
{
    static uint32_t cnt = 1;

    lv_obj_t *btn = lv_event_get_target(e);
    lv_obj_t *label = lv_obj_get_child(btn, 0); // 6.获取标签

    lv_label_set_text_fmt(label, "%" LV_PRIu32, cnt); // 7.更新标签内容

    cnt++;
}