#include <stdio.h>
#include "sum.h"
#include "add.h"
#include "sub.h"

void SUM(void)
{
    printf("SUM begin!\n");
    Add();
    Sub();
    printf("SUM end!\n");
}
