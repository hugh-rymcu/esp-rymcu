#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/*************************************************************************************
 *
 * ���ܣ���ʾstyle-line
 * styles are used to set the appearance of objects,a style is an lv_style_t variable
 *
 * 1) styles are stored in lv_style_t variables
 * 2) style variables should be static,global or dynamically allcocated
 * 3) style variables in functions will be destroyed after the functon exits
 * 4) before using a styte,should be iniitialized with lv_style_init(&my_style)
 *
 * initialize: lv_style_init()
 *        set: lv_style_set_<property_name>()
 *        get: lv_style_get_<property_name>()
 *      reset: lv_style_reset()
 *
 *        add: lv_obj_add_style
 *     remove: lv_style_remove_prop()
 * remove all:lv_obj_remove_style_all()
 *    repalce: lv_obj_replace_style()
 *
 **************************************************************************************/

void my_gui(void)
{
    static lv_style_t style;
    lv_style_init(&style);
    lv_style_set_line_color(&style, lv_palette_main(LV_PALETTE_RED));
    lv_style_set_line_width(&style, 6);
    lv_style_set_line_rounded(&style, true);
    /*Create an object with the new style*/
    lv_obj_t *obj = lv_line_create(lv_scr_act());
    lv_obj_add_style(obj, &style, 0);
    static lv_point_t p[] = {{10, 30}, {30, 50}, {100, 0}};
    lv_line_set_points(obj, p, 3);
    lv_obj_center(obj);
}