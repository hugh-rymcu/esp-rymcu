/*
 * SPDX-FileCopyrightText: 2010-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: CC0-1.0
 */

#include <stdio.h>
#include <inttypes.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_chip_info.h"
#include "esp_flash.h"

#include "lv_port_indev_template.h"
#include "lv_port_disp_template.h"
#include "lvgl.h"

void app_main(void)
{
    lv_init();//LVGL初始化
    lv_port_indev_init();//触摸初始化
    lv_port_disp_init();//显示器初始化
   
    while (1)
    {
        printf("Hello world!\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS); // delay 1s
    }
}
