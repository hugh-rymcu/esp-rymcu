#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"

lv_obj_t *list;
static void event_cb(lv_event_t *e)
{
    printf("btn clicked!\r\n");
}

void my_gui(void)
{
    list = lv_list_create(lv_scr_act()); // 1.创建list部件
    lv_obj_set_size(list, 320, 240);     // 2.设置部件大小
    lv_obj_center(list);                 // 3.居中对齐

    lv_list_add_text(list, "settings");                             // 4.添加部件文本
    lv_obj_t *btn1 = lv_list_add_btn(list, LV_SYMBOL_WIFI, "WLAN"); // 5.添加部件按钮,包含LV自带图标
    lv_obj_t *btn2 = lv_list_add_btn(list, LV_SYMBOL_DRIVE, "DRIVe");
    lv_obj_t *btn3 = lv_list_add_btn(list, LV_SYMBOL_MUTE, "MUTE");
    lv_obj_t *btn4 = lv_list_add_btn(list, LV_SYMBOL_VOLUME_MID, "MID");
    lv_obj_t *btn5 = lv_list_add_btn(list, LV_SYMBOL_STOP, "STOP");
    lv_obj_t *btn6 = lv_list_add_btn(list, LV_SYMBOL_LOOP, "LOOP");
    lv_obj_t *btn7 = lv_list_add_btn(list, LV_SYMBOL_KEYBOARD, "KEYBOARD");
    lv_obj_t *btn8 = lv_list_add_btn(list, LV_SYMBOL_ENVELOPE, "ENVELOPE");

    lv_obj_add_event_cb(btn1, event_cb, LV_EVENT_CLICKED, NULL); // 6.添加事件回掉函数
}