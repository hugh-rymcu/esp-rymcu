#ifndef __ST7789_DRIVER_H__
#define __ST7789_DRIVER_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

#define LCD_HOST    SPI2_HOST

#define PIN_NUM_MISO 13
#define PIN_NUM_MOSI 11
#define PIN_NUM_CLK  12
#define PIN_NUM_CS   10  

#define PIN_NUM_DC   9
#define PIN_NUM_RST  3
#define PIN_NUM_BCKL 46

#define LCD_Width 	240
#define LCD_Height 	320//320

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F//0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红�?
#define GRAY  			 0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝�?
#define LIGHTBLUE      	 0X7D7C	//浅蓝�?  
#define GRAYBLUE       	 0X5458 //灰蓝�?
//以上三色为PANEL的颜�? 
 
#define LIGHTGREEN     	 0X841F //浅绿�?
//#define LIGHTGRAY        0XEF5B //浅灰�?(PANNEL)
#define LGRAY 			 0XC618 //浅灰�?(PANNEL),窗体背景�?

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜�?)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反�?)

//To speed up transfers, every SPI transfer sends a bunch of lines. This define specifies how many. More means more memory use,
//but less overhead for setting up / finishing transfers. Make sure 240 is dividable by this.
#define PARALLEL_LINES 16

/*
 The LCD needs a bunch of command/argument values to be initialized. They are stored in this struct.
*/
typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} lcd_init_cmd_t;

typedef enum {
    LCD_TYPE_ILI = 1,
    LCD_TYPE_ST,
    LCD_TYPE_MAX,
} type_lcd_t;

extern uint16_t g_point_color;	//画笔颜色	默认为黑�?
extern uint16_t g_bc_point_color;	//背景颜色	默认为白�?


void lcd_init(void);
void display_pretty_colors(void);
void lcd_clear(uint16_t color);

void lcd_set_color(uint16_t color);
uint16_t lcd_get_color(void);
void lcd_draw_point(uint16_t x, uint16_t y);
void lcd_draw_big_point(uint16_t x, uint16_t y);
void lcd_draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void lcd_draw_rectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void lcd_draw_circle(uint16_t x0, uint16_t y0, uint8_t r);

#endif