#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/*************************************************************************************
 *
 * 功能：演示style
 * styles are used to set the appearance of objects,a style is an lv_style_t variable
 *
 * 1) styles are stored in lv_style_t variables
 * 2) style variables should be static,global or dynamically allcocated
 * 3) style variables in functions will be destroyed after the functon exits
 * 4) before using a styte,should be iniitialized with lv_style_init(&my_style)
 *
 * initialize: lv_style_init()
 *        set: lv_style_set_<property_name>()
 *        get: lv_style_get_<property_name>()
 *      reset: lv_style_reset()
 *
 *        add: lv_obj_add_style
 *     remove: lv_style_remove_prop()
 * remove all:lv_obj_remove_style_all()
 *    repalce: lv_obj_replace_style()
 *
 **************************************************************************************/

void my_gui(void)
{
    static lv_style_t style;
    lv_style_init(&style); // 1.初始化style

    lv_style_set_radius(&style, 50); // 2.设置style
    lv_style_set_width(&style, 150);
    lv_style_set_height(&style, LV_SIZE_CONTENT);
    lv_style_set_pad_ver(&style, 20);
    lv_style_set_pad_left(&style, 5);
    lv_style_set_x(&style, lv_pct(25));
    lv_style_set_y(&style, 80);
    /*Create an object with the new style*/
    lv_obj_t *obj = lv_obj_create(lv_scr_act());
    lv_obj_add_style(obj, &style, 0); // 3.添加style

    lv_obj_t *label = lv_label_create(obj);
    lv_label_set_text(label, "rymcu.com");
}
