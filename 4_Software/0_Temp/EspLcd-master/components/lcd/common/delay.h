#ifndef DELAY_H
#define DELAY_H

int get_sys_ms(void);

int delay_ms(int ms);

#endif
