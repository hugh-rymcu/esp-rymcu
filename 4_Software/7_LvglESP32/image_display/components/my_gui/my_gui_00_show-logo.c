#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/*************************************************************************************
 *
 * 功能：演示animation,palyback
 *  红球来回运动
 *
 **************************************************************************************/
LV_IMG_DECLARE(LOGO_RYMCU); // 2.图片声明

static void anim_x_cb(void *var, int32_t v);
static void anim_size_cb(void *var, int32_t v);

void my_gui(void)
{
    lv_obj_t *img = lv_img_create(lv_scr_act()); // 1.创建图片部件
    lv_img_set_src(img, &LOGO_RYMCU);            // 2.设置图片源
    lv_obj_center(img);                          // 图片居中

    lv_img_set_offset_x(img, 0); // 3.图片偏移
    lv_img_set_offset_y(img, 0);

    lv_anim_t a; // 2.创建动画并关联部件obj
    lv_anim_init(&a);
    lv_anim_set_var(&a, img);

    lv_anim_set_time(&a, 2000); // 3.动画前进持续时间

    lv_anim_set_playback_delay(&a, 100); // 4.动画前进到目的地后，延迟一段时间再返回
    lv_anim_set_playback_time(&a, 2000); // 5.动画返回持续时间

    lv_anim_set_repeat_delay(&a, 100);                     // 6.动画完成前进返回动作后，延迟一段时间
    lv_anim_set_repeat_count(&a, LV_ANIM_REPEAT_INFINITE); // 7.动画重复时间
    lv_anim_set_path_cb(&a, lv_anim_path_bounce);          // 8.设置动画撞墙特效

    lv_anim_set_values(&a, 256, 400); // 9.设置开始，结束值传入函数anim_size_cb
    lv_anim_set_exec_cb(&a, anim_size_cb);
    lv_anim_start(&a); // 10.启动动画

    lv_anim_set_values(&a, 0, 30); // 11.设置开始、结束值传入函数anim_x_cb
    lv_anim_set_exec_cb(&a, anim_x_cb);
    lv_anim_start(&a);
}
static void anim_size_cb(void *var, int32_t v)
{
    lv_img_set_zoom(var, v); // 5.图片缩放，128=1/2，256=1/1，512=2/1
}
static void anim_x_cb(void *var, int32_t v)
{
    lv_obj_set_y(var, v); // obj的x坐标变化
}
