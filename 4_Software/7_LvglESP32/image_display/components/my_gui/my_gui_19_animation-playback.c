#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/*************************************************************************************
 *
 * 功能：演示animation,palyback
 *  红球来回运动
 *
 **************************************************************************************/
static void anim_x_cb(void *var, int32_t v);
static void anim_size_cb(void *var, int32_t v);

    void my_gui(void)
{
    lv_obj_t *obj = lv_obj_create(lv_scr_act());//1.创建红色圆形部件obj
    lv_obj_set_style_bg_color(obj, lv_palette_main(LV_PALETTE_RED), 0);
    lv_obj_set_style_radius(obj, LV_RADIUS_CIRCLE, 0);
    lv_obj_align(obj, LV_ALIGN_LEFT_MID, 10, 0);

    lv_anim_t a;//2.创建动画并关联部件obj
    lv_anim_init(&a);
    lv_anim_set_var(&a, obj);

    lv_anim_set_time(&a, 1000);//3.动画前进持续时间

    lv_anim_set_playback_delay(&a, 1000);//4.动画前进到目的地后，延迟一段时间再返回
    lv_anim_set_playback_time(&a, 500);//5.动画返回持续时间

    lv_anim_set_repeat_delay(&a, 1000);//6.动画完成前进返回动作后，延迟一段时间
    lv_anim_set_repeat_count(&a, LV_ANIM_REPEAT_INFINITE);//7.动画重复时间
    lv_anim_set_path_cb(&a, lv_anim_path_bounce);//8.设置动画撞墙特效

    lv_anim_set_values(&a, 10, 100);//9.设置开始，结束值传入函数anim_size_cb
    lv_anim_set_exec_cb(&a, anim_size_cb);
    lv_anim_start(&a);//10.启动动画
   
    lv_anim_set_values(&a, 10, 240);//11.设置开始、结束值传入函数anim_x_cb
    lv_anim_set_exec_cb(&a, anim_x_cb);
    lv_anim_start(&a);

}
static void anim_size_cb(void *var, int32_t v)
{
    lv_obj_set_size(var, v, v);//obj的size变化
}
static void anim_x_cb(void *var, int32_t v)
{
    lv_obj_set_x(var, v);//obj的x坐标变化
}
