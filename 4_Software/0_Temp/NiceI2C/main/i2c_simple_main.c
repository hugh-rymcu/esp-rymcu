/* i2c - Simple example

   Simple I2C example that shows how to initialize I2C
   as well as reading and writing from and to registers for a sensor connected over I2C.

   The sensor used in this example is a MPU9250 inertial measurement unit.

   For other examples please check:
   https://github.com/espressif/esp-idf/tree/master/examples

   See README.md file to get detailed usage of this example.

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "esp_log.h"
#include "driver/i2c.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "oled.h"
#include "bmp.h"

static const char *TAG = "i2c-simple-example";

#define I2C_MASTER_SCL_IO CONFIG_I2C_MASTER_SCL /*!< GPIO number used for I2C master clock */
#define I2C_MASTER_SDA_IO CONFIG_I2C_MASTER_SDA /*!< GPIO number used for I2C master data  */
#define I2C_MASTER_NUM 0                        /*!< I2C master i2c port number, the number of i2c peripheral interfaces available will depend on the chip */
#define I2C_MASTER_FREQ_HZ 400000               /*!< I2C master clock frequency */
#define I2C_MASTER_TX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0             /*!< I2C master doesn't need buffer */
#define I2C_MASTER_TIMEOUT_MS 1000

#define MPU9250_SENSOR_ADDR 0x68       /*!< Slave address of the MPU9250 sensor */
#define MPU9250_WHO_AM_I_REG_ADDR 0x75 /*!< Register addresses of the "who am I" register */

#define MPU9250_PWR_MGMT_1_REG_ADDR 0x6B /*!< Register addresses of the power managment register */
#define MPU9250_RESET_BIT 7

/**
 * @brief i2c master initialization
 */
static esp_err_t i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;

    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = I2C_MASTER_SDA_IO,
        .scl_io_num = I2C_MASTER_SCL_IO,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = I2C_MASTER_FREQ_HZ,
    };

    i2c_param_config(i2c_master_port, &conf);

    return i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
}

void app_main(void)
{
    ESP_ERROR_CHECK(i2c_master_init());
    ESP_LOGI(TAG, "I2C initialized successfully");

    OLED_Init();       // ��ʼ��OLED
    OLED_Clear();      // �����Ļ
    OLED_Display_On(); // ����OLED
    /*****************************************
     *
     *0.96 OLED �ַ���ʾ����
     *
     *******************************************/
    OLED_ShowChar(0, 0, 'A', 16, 0);
    OLED_ShowChar(8, 0, 'B', 16, 0);
    OLED_ShowChar(16, 0, 'C', 16, 0);
    OLED_ShowChar(24, 0, 'D', 16, 0);

    OLED_ShowChar(0, 2, 'A', 8, 0);
    OLED_ShowChar(8, 2, 'B', 8, 0);
    OLED_ShowChar(16, 2, 'C', 8, 0);
    OLED_ShowChar(24, 2, 'D', 8, 0);

    OLED_ShowString(25, 6, (unsigned char*)"Char Test!", 16, 1);

    vTaskDelay(1000 / portTICK_PERIOD_MS); // delay 1s
    OLED_Clear();                          // �����Ļ

    /*****************************************
     *
     *0.96 OLED ������ʾ����
     *
     *******************************************/

    OLED_ShowNum(0, 1, 12, 2, 16, 0);
    OLED_ShowNum(48, 1, 34, 2, 16, 0);
    OLED_ShowNum(96, 1, 56, 2, 16, 0);

    OLED_ShowString(25, 6, (unsigned char*)"Num Test!", 16, 1);

    vTaskDelay(1000 / portTICK_PERIOD_MS); // delay 1s
    OLED_Clear();                          // �����Ļ

    /*****************************************
     *
     *0.96 OLED ������ʾ����
     *
     *******************************************/
    OLED_ShowCHinese(22, 3, 1, 0);      // ��
    OLED_ShowCHinese(22 + 16, 3, 2, 0); // ��
    OLED_ShowCHinese(22 + 32, 3, 3, 0); // ��
    OLED_ShowCHinese(22 + 48, 3, 4, 0); // ɢ
    OLED_ShowCHinese(22 + 64, 3, 5, 0); // ��

    OLED_ShowString(25, 6, (unsigned char*)"CHN Test!", 16, 1);

    vTaskDelay(1000 / portTICK_PERIOD_MS); // delay 1s
    OLED_Clear();                          // �����Ļ

    /*****************************************
     *
     *0.96 OLED �ַ�����ʾ����
     *
     *******************************************/

    OLED_ShowString(0, 2, (unsigned char*)"Nebula-Pi,RYMCU!", 16, 0);

    OLED_ShowString(25, 6, (unsigned char*)"Str Test!", 16, 1);

    vTaskDelay(1000 / portTICK_PERIOD_MS); // delay 1s
    OLED_Clear();                          // �����Ļ
    /*****************************************
     *
     *0.96 OLED �ַ�����ʾ����
     *
     *******************************************/

    OLED_DrawBMP(0, 0, Logo, 0); // ��ʾͼƬ

    OLED_ShowString(25, 6, (unsigned char*)"PIC Test!", 16, 1);
    vTaskDelay(1000 / portTICK_PERIOD_MS); // delay 1s

    ESP_ERROR_CHECK(i2c_driver_delete(I2C_MASTER_NUM));
    ESP_LOGI(TAG, "I2C de-initialized successfully");
}
