/* SPI Master example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

#include "pretty_effect.h"

#include "alientek_log.h"
#include "st7789_driver.h"

void app_main(void)
{
    // Initialize the LCD
    lcd_init();
    // Initialize the effect displayed
    // ret=pretty_effect_init();
    // ESP_ERROR_CHECK(ret);

    // Go do nice stuff.
    // display_pretty_colors();

    lcd_clear(WHITE);

    lcd_set_color(BLACK);
    lcd_draw_big_point(120, 160);
    lcd_draw_circle(120,160,50);
    lcd_draw_line(0,0,240,320);

    while (1)
    {
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}
