#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/**********************************************************************************
 *
 * 功能：演示部件事件bubble功能
 * 创建30个btn，开通bubble功能，点击btn则改变颜色，并答应btn序号。点击容器时，不处理。
 *
 **********************************************************************************/
void event_cb(lv_event_t *e);

void my_gui(void)
{
    lv_obj_t *cont = lv_obj_create(lv_scr_act()); // 1.创建容器cont，并设定排序规则
    lv_obj_set_size(cont, 290, 200);
    lv_obj_center(cont);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_ROW_WRAP);

    for (uint32_t i = 0; i < 30; i++) // 2.在cont上创建30个btn部件
    {
        lv_obj_t *btn = lv_btn_create(cont);
        lv_obj_set_size(btn, 70, 50);
        lv_obj_add_flag(btn, LV_OBJ_FLAG_EVENT_BUBBLE); // 3.使能Bubble

        lv_obj_t *label = lv_label_create(btn);
        lv_label_set_text_fmt(label, "%" LV_PRIu32, i);
        lv_obj_center(label);
    }
    lv_obj_add_event_cb(cont, event_cb, LV_EVENT_CLICKED, NULL); // 4.添加点击回调函数
}

void event_cb(lv_event_t *e) // 5.事件回调函数
{
    lv_obj_t *target = lv_event_get_target(e);       // 6.获取原始部件事件，来自btn或cont
    lv_obj_t *cont = lv_event_get_current_target(e); // 7.当前容器事件,来自容器，因使能了Bubble

    lv_obj_t *label_temp = NULL;

    if (target == cont) // 8.相同时，表示事件来自容器,即点击了容器而不是btns
    {
        printf("event not from btns,but from container\r\n");
        return;
    }
    lv_obj_set_style_bg_color(target, lv_palette_main(LV_PALETTE_RED), 0); // 8.按键改为红色

    label_temp = lv_obj_get_child(target, 0); // 9.获取标签,并打印显示。
    char *str = lv_label_get_text(label_temp);
    printf("btn %s clicked!\r\n", str);
}