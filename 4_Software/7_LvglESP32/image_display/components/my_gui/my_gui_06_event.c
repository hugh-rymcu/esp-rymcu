#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/**********************************************************************************
 *
 * 功能：演示部件事件
 * 点击按钮，按钮标签数字显示+1
 *
 **********************************************************************************/
void event_cb(lv_event_t *e);

void my_gui(void)
{
    lv_obj_t *btn = lv_btn_create(lv_scr_act()); // 1.创建btn部件
    lv_obj_set_size(btn, 100, 50);
    lv_obj_center(btn);

    lv_obj_t *label = lv_label_create(btn); // 2.创建标签，并关联btn
    lv_label_set_text(label, "Click me!");
    lv_obj_center(label);

    lv_obj_add_event_cb(btn, event_cb, LV_EVENT_CLICKED, NULL); // 3.添加点击事件
}

void event_cb(lv_event_t *e) // 5.点击事件回调函数
{
    static uint32_t cnt = 1;

    lv_obj_t *btn = lv_event_get_target(e);
    lv_obj_t *label = lv_obj_get_child(btn, 0); // 6.获取标签

    lv_label_set_text_fmt(label, "%" LV_PRIu32, cnt); // 7.更新标签内容

    cnt++;
}