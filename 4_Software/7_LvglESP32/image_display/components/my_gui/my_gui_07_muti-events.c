#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/**********************************************************************************
 *
 * 功能：演示部件事件
 * 多事件处理，将实现以文本显示显示在屏幕上。
 *
 **********************************************************************************/
void event_cb(lv_event_t *e);

void my_gui(void)
{
    lv_obj_t *btn = lv_btn_create(lv_scr_act()); // 1.创建btn部件
    lv_obj_set_size(btn, 100, 50);
    lv_obj_center(btn);

    lv_obj_t *label = lv_label_create(btn); // 2.创建标签，并关联btn
    lv_label_set_text(label, "Click me!");
    lv_obj_center(label);

    lv_obj_t *info_label = lv_label_create(lv_scr_act()); // 3.创建主屏幕标签
    lv_label_set_text(info_label, "The last button event:\nNone");

    lv_obj_add_event_cb(btn, event_cb, LV_EVENT_ALL, info_label); // 4.添加btn事件
}

void event_cb(lv_event_t *e) // 5.事件回调函数
{
    lv_event_code_t code = lv_event_get_code(e); // 5.获取事件代码
    lv_obj_t *label = lv_event_get_user_data(e); // 6.获取用户数据，即获取info_label
    switch (code)
    {
    case LV_EVENT_PRESSED: // 按下
        lv_label_set_text(label, "The last button event:\nLV_EVENT_PRESSED");
        break;
    case LV_EVENT_CLICKED: // 点击，包括按下，谈弹起
        lv_label_set_text(label, "The last button event:\nLV_EVENT_CLICKED");
        break;
    case LV_EVENT_LONG_PRESSED: // 长按
        lv_label_set_text(label, "The last button event:\nLV_EVENT_LONG_PRESSED");
        break;
    case LV_EVENT_LONG_PRESSED_REPEAT: // 重复长按
        lv_label_set_text(label, "The last button event:\nLV_EVENT_LONG_PRESSED_REPEAT");
        break;
    default:
        break;
    }
}