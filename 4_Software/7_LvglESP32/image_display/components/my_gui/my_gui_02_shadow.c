#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"

void my_gui(void)
{
    static lv_style_t style_shadow;//1.设置边框阴影样式

    lv_style_init(&style_shadow);
    lv_style_set_shadow_width(&style_shadow, 10);
    lv_style_set_shadow_spread(&style_shadow, 5);
    lv_style_set_shadow_color(&style_shadow, lv_palette_main(LV_PALETTE_BLUE));

    lv_obj_t *obj1;
    obj1 = lv_obj_create(lv_scr_act());//2.创建部件，并添加阴影
    lv_obj_add_style(obj1, &style_shadow, 0);
    lv_obj_align(obj1, LV_ALIGN_CENTER, 0, 0);
}
