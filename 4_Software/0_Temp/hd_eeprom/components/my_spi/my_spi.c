
#include "my_spi.h"

spi_device_handle_t my_spi_1;

spi_bus_config_t buscfg = {
    .miso_io_num = PIN_NUM_MISO, // MISO信号线
    .mosi_io_num = PIN_NUM_MOSI, // MOSI信号线
    .sclk_io_num = PIN_NUM_CLK,  // SCLK信号线
    .quadwp_io_num = -1,         // WP信号线，专用于QSPI的D2
    .quadhd_io_num = -1,         // HD信号线，专用于QSPI的D3
    .max_transfer_sz = 64 * 8,   // 最大传输数据大小
};
spi_device_interface_config_t devcfg = {
    .clock_speed_hz = SPI_MASTER_FREQ_10M, // Clock out at 10 MHz,
    .mode = 0,                             // SPI mode auto
    .spics_io_num = PIN_NUM_CS,
    .queue_size = 7, // 传输队列大小，决定了等待传输数据的数量
};

void init_my_spi(void)
{
    esp_err_t ret;
    ret = spi_bus_initialize(MY_SPI, &buscfg, DMA_CHAN);
    ESP_ERROR_CHECK(ret);

    ret = spi_bus_add_device(MY_SPI, &devcfg, &my_spi_1);
    ESP_ERROR_CHECK(ret);

   // esp_rom_gpio_pad_select_gpio(PIN_NUM_CS);        // 选择一个GPIO
    //gpio_set_direction(PIN_NUM_CS, GPIO_MODE_OUTPUT); // 把这个GPIO作为输出
}

esp_err_t spi_write(spi_device_handle_t my_spi, uint8_t *data, uint8_t len)
{
    esp_err_t ret = 0;
    spi_transaction_t t;
    if (len == 0)
        return ret;           // no need to send anything
    memset(&t, 0, sizeof(t)); // Zero out the transaction

    gpio_set_level(PIN_NUM_CS, 0);

    t.length = len * 8;                         // Len is in bytes, transaction length is in bits.
    t.tx_buffer = data;                         // Data
    t.user = (void *)1;                         // D/C needs to be set to 1
    ret = spi_device_polling_transmit(my_spi, &t); // Transmit!
    assert(ret == ESP_OK);                      // Should have had no issues.

    gpio_set_level(PIN_NUM_CS, 1);
    return ret;
}

esp_err_t spi_read(spi_device_handle_t my_spi, uint8_t *data)
{
    spi_transaction_t t;
    esp_err_t ret = 0;
    gpio_set_level(PIN_NUM_CS, 0);

    memset(&t, 0, sizeof(t));
    t.length = 8;
    t.flags = SPI_TRANS_USE_RXDATA;
    t.user = (void *)1;
    spi_device_polling_transmit(my_spi, &t);

    *data = t.rx_data[0];

    gpio_set_level(PIN_NUM_CS, 1);

    return ret;
}

void my_spi_test(void)
{

    esp_err_t ret;
    char test_str[] = "Hello!";
    uint8_t test_buf[4] = "";

    init_my_spi();

    while (1)
    {
        spi_write(my_spi_1, (uint8_t *)test_str, 13);
        ESP_LOGI(TAG, "Write: %s", test_str);
        vTaskDelay(100);

        for (int i = 0; i < sizeof(test_buf); i++)
        {
            ret = spi_read(my_spi_1, &test_buf[i]);
            ESP_ERROR_CHECK(ret);
        }
        ESP_LOGI(TAG, "Read: %s", test_buf);
        memset(test_buf, 0, 4);
        vTaskDelay(100);
    }
}
