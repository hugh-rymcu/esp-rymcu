/* SPI Master Half Duplex EEPROM example.

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

#include "sdkconfig.h"
#include "esp_log.h"
#include "spi_eeprom.h"
#include "my_spi.h"
#include "lcd.h"
#include "lcd_init.h"
/*
 This code demonstrates how to use the SPI master half duplex mode to read/write a AT932C46D EEPROM (8-bit mode).
*/

// #define EEPROM_HOST SPI2_HOST

// #define PIN_NUM_MISO 13
// #define PIN_NUM_MOSI 11
// #define PIN_NUM_CLK 12
// #define PIN_NUM_CS 10

void app_main(void)
{
    init_my_spi();
    LCD_Init();

    LCD_Fill(0,0,LCD_W,LCD_H,BLUE);
    //my_spi_test();

    // /////////////////////////////////////////////////////////////////////
    // // 设置并初始化 主机SPI BUS
    // /*
    // * 1.spi_bus_initialize()   set spi_bus_config_t
    // * 2.spi_bus_add_device()
    // * 3.spi_device_polling_transmit() ,polling
    // */
    // esp_err_t ret;
    // spi_bus_config_t buscfg={
    //     .miso_io_num = PIN_NUM_MISO,
    //     .mosi_io_num = PIN_NUM_MOSI,
    //     .sclk_io_num = PIN_NUM_CLK,
    //     .quadwp_io_num = -1,
    //     .quadhd_io_num = -1,
    //     .max_transfer_sz = 32,
    // };
    // //Initialize the SPI bus
    // spi_bus_initialize(EEPROM_HOST, &buscfg, SPI_DMA_CH_AUTO);

    // /////////////////////////////////////////////////////////////////////
    // // 设置并初始化 SPI BUS
    // eeprom_config_t eeprom_config = {
    //     .cs_io = PIN_NUM_CS,
    //     .host = EEPROM_HOST,
    //     .miso_io = PIN_NUM_MISO,
    // };

    // eeprom_handle_t eeprom_handle;

    // spi_eeprom_init(&eeprom_config, &eeprom_handle);
    // spi_eeprom_write_enable(eeprom_handle);

    // const char test_str[] = "Hello World!";
    // ESP_LOGI(TAG, "Write: %s", test_str);
    // for (int i = 0; i < sizeof(test_str); i++) {
    //     // No need for this EEPROM to erase before write.
    //     ret = spi_eeprom_write(eeprom_handle, i, test_str[i]);
    //     ESP_ERROR_CHECK(ret);
    // }

    // uint8_t test_buf[32] = "";
    // for (int i = 0; i < sizeof(test_str); i++) {
    //     ret = spi_eeprom_read(eeprom_handle, i, &test_buf[i]);
    //     ESP_ERROR_CHECK(ret);
    // }
    // ESP_LOGI(TAG, "Read: %s", test_buf);

    // ESP_LOGI(TAG, "Example finished.");

    // while (1) {
    //     // Add your main loop handling code here.
    //     vTaskDelay(1);
    // }
}
