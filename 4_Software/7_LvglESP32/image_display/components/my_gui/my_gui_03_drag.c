#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"

static void drag_event_handler(lv_event_t *e); // 3.事件回掉函数

void my_gui(void)
{
    lv_obj_t *obj;
    obj = lv_obj_create(lv_scr_act());//1.创建部件，并添加按压事件
    lv_obj_set_size(obj, 150, 100);
    lv_obj_add_event_cb(obj, drag_event_handler, LV_EVENT_PRESSING, NULL);

    lv_obj_t *label = lv_label_create(obj);//2.创建标签，并添加至obj部件
    lv_label_set_text(label, "Drag me");
    lv_obj_center(label);
}

static void drag_event_handler(lv_event_t *e)//4.拖拽回掉处理函数
{
    lv_obj_t *obj = lv_event_get_target(e); // 5.获取事件对象
    lv_indev_t *indev = lv_indev_get_act(); // 6.获取当前输入设备
    if (indev == NULL) return;

    lv_point_t vect;
    lv_indev_get_vect(indev, &vect); // 7.获取输入设备indev的运动向量
    lv_coord_t x = lv_obj_get_x(obj) + vect.x;
    lv_coord_t y = lv_obj_get_y(obj) + vect.y;
    lv_obj_set_pos(obj, x, y); // 7.移动对象到指定位置
    
    printf("x:%d,y:%d\r\n",x,y);//打印当前坐标
}