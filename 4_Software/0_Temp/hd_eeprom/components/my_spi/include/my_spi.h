#ifndef _MY_SPI_H
#define _MY_SPI_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

#include "sdkconfig.h"
#include "esp_log.h"

#define MY_SPI SPI2_HOST
#define DMA_CHAN SPI_DMA_CH_AUTO
#define PIN_NUM_MISO 37
#define PIN_NUM_MOSI 35
#define PIN_NUM_CLK  36
#define PIN_NUM_CS   45

#define PIN_NUM_DC  4
#define PIN_NUM_RES 5
#define PIN_NUM_BLK 6

#define GPIO_OUTPUT_PIN_SEL ((1ULL << PIN_NUM_RES) | (1ULL << PIN_NUM_DC)| (1ULL << PIN_NUM_BLK)| (1ULL << PIN_NUM_CS))


extern spi_device_handle_t my_spi_1;
static const char TAG[] = "my_spi";
void init_my_spi(void);
esp_err_t spi_write(spi_device_handle_t my_spi, uint8_t *data, uint8_t len);
esp_err_t spi_read(spi_device_handle_t my_spi, uint8_t *data);
void my_spi_test(void);

#endif