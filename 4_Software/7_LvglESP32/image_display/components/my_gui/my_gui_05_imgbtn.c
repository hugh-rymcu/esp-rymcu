#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/**********************************************************************************
 * 图片数组.c文件生成方法：
 * 1.准备.png图片一透明张,注意是透明图，改变颜色的时候，背景不受影响！！！
 * 2.访问https://lvgl.io/，菜单栏找到Tools->image conventer
 * 3.点击Browse上传图片，color format设置为CF_TRUE_COLOR_ALPHA,Output format选择C array
 * 4.点击Convert,等待.c文件生成并添加到工程目录。
 **********************************************************************************/
LV_IMG_DECLARE(fire); // 1.图片声明

static lv_obj_t *imgbtn; // 2.声明图片按钮部件

static void event_cb(lv_event_t *e); // 每次点击改变图片颜色

void my_gui(void)
{
    imgbtn = lv_imgbtn_create(lv_scr_act());                                // 3.创建imgbtn部件
    lv_imgbtn_set_src(imgbtn, LV_IMGBTN_STATE_RELEASED, NULL, &fire, NULL); // 4.设置imgbtn图片
    lv_obj_set_size(imgbtn, 200, 200);                                      // 5.设置imgbtn部件尺寸，注意与图片匹配
    lv_obj_center(imgbtn);

    lv_obj_add_event_cb(imgbtn, event_cb, LV_EVENT_PRESSED, NULL); // 6.添加按压事件
}

static void event_cb(lv_event_t *e)
{
    static int i = 0;

    lv_obj_set_style_img_recolor(imgbtn, lv_color_hex(0xd81e06), LV_STATE_DEFAULT);//7.设置为红色

    if (i % 2 == 0)
        lv_obj_set_style_img_recolor_opa(imgbtn, 255, LV_STATE_DEFAULT);//选择透明度，实现颜色改变
    else
        lv_obj_set_style_img_recolor_opa(imgbtn, 0, LV_STATE_DEFAULT);

    i++;
    printf("times:%d\r\n", i);//8.打印按压次数
}