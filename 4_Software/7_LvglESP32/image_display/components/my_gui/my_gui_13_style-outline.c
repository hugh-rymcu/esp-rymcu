#include "my_gui.h"
#include "lvgl.h"
#include "stdio.h"
/*************************************************************************************
 *
 * ���ܣ���ʾstyle-outline
 * styles are used to set the appearance of objects,a style is an lv_style_t variable
 *
 * 1) styles are stored in lv_style_t variables
 * 2) style variables should be static,global or dynamically allcocated
 * 3) style variables in functions will be destroyed after the functon exits
 * 4) before using a styte,should be iniitialized with lv_style_init(&my_style)
 *
 * initialize: lv_style_init()
 *        set: lv_style_set_<property_name>()
 *        get: lv_style_get_<property_name>()
 *      reset: lv_style_reset()
 *
 *        add: lv_obj_add_style
 *     remove: lv_style_remove_prop()
 * remove all:lv_obj_remove_style_all()
 *    repalce: lv_obj_replace_style()
 *
 **************************************************************************************/

void my_gui(void)
{
    static lv_style_t style;
    lv_style_init(&style);
    /*Set a background color and a radius*/
    lv_style_set_radius(&style, 5);
    lv_style_set_bg_opa(&style, LV_OPA_COVER);
    lv_style_set_bg_color(&style, lv_palette_lighten(LV_PALETTE_GREY, 1));
    /*Add outline*/
    lv_style_set_outline_width(&style, 2);
    lv_style_set_outline_color(&style, lv_palette_main(LV_PALETTE_BLUE));
    lv_style_set_outline_pad(&style, 8);
    /*Create an object with the new style*/
    lv_obj_t *obj = lv_obj_create(lv_scr_act());
    lv_obj_add_style(obj, &style, 0);
    lv_obj_center(obj);
}