#ifndef __LCD_INIT_H
#define __LCD_INIT_H

#include "sys.h"
#include "my_spi.h"

#define USE_HORIZONTAL 0//设置横屏或者竖屏显示 0或1为竖屏 2或3为横屏


#if USE_HORIZONTAL==0||USE_HORIZONTAL==1
#define LCD_W 240
#define LCD_H 280

#else
#define LCD_W 280
#define LCD_H 240
#endif




//-----------------LCD端口定义---------------- 

#define LCD_SCLK_Clr()   gpio_set_level(PIN_NUM_CLK, 0)
#define LCD_SCLK_Set()   gpio_set_level(PIN_NUM_CLK, 1)

#define LCD_MOSI_Clr() gpio_set_level(PIN_NUM_MOSI, 0)//SDA=MOSI
#define LCD_MOSI_Set() gpio_set_level(PIN_NUM_MOSI, 1)

#define LCD_RES_Clr()  gpio_set_level(PIN_NUM_RES, 0)//RES
#define LCD_RES_Set()  gpio_set_level(PIN_NUM_RES, 1)

#define LCD_DC_Clr()   gpio_set_level(PIN_NUM_DC, 0)//DC
#define LCD_DC_Set()   gpio_set_level(PIN_NUM_DC, 1)
 		     
#define LCD_CS_Clr()   gpio_set_level(PIN_NUM_CS, 0)//CS
#define LCD_CS_Set()   gpio_set_level(PIN_NUM_CS, 1)

#define LCD_BLK_Clr()  gpio_set_level(PIN_NUM_BLK, 0)/BLK
#define LCD_BLK_Set()  gpio_set_level(PIN_NUM_BLK, 1)



void delay_ms(uint8_t ms);
void LCD_GPIO_Init(void);//初始化GPIO
void LCD_Writ_Bus(u8 dat);//模拟SPI时序
void LCD_WR_DATA8(u8 dat);//写入一个字节
void LCD_WR_DATA(u16 dat);//写入两个字节
void LCD_WR_REG(u8 dat);//写入一个指令
void LCD_Address_Set(u16 x1,u16 y1,u16 x2,u16 y2);//设置坐标函数
void LCD_Init(void);//LCD初始化
#endif




